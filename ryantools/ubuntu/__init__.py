"""Ryan's personal Python library

Package for Ubuntu (18.04)

(C) Ryan James Decker All Rights Reserved

Description:
-----------
This is my ubuntu package for Python.

For now, it just has some utilities for 
customizing all the start-up themes, like 
for the boot menu, boot splash, login 
screen, lock screen, and desktop background.

Included Modules:
----------------
  -grub2.py
  -plymouth.py
  -gdm.py
  
Included Packages:
-----------------
None
  
"""
