'''
'''

import os, subprocess

def set_desktop_bg(fullpath):

    subprocess.call([
        'gsettings',
        'set',
        'org.gnome.desktop.background',
        'picture-uri',
        'file:///{}'.format(fullpath)
    ])

def set_lockscreen_bg(fullpath):

    subprocess.call([
        'gsettings',
        'set',
        'org.gnome.desktop.screensaver',
        'picture-uri',
        'file:///{}'.format(fullpath)
    ])

