"""My gdm module for Python

(C) Ryan James Decker All Rights Reserved

Nothing much to document here yet. When 
  it begins to grow, I'll update 
  accordingly.

"""

import cssutils, subprocess

_GDM_BG_FILEPATH = r'/usr/share/gnome-shell/theme/dm-background.jpg'

def _get_file_contents(path):
    
    with open(path, 'r') as file:
        
        file_contents = file.read()
        
    return file_contents

def _update_login_bg_css():
    
    original_rule = \
      '''#lockDialogGroup {
      background: #2c001e url(resource:///org/gnome/shell/theme/noise-texture.png);
      background-repeat: repeat; }'''

    new_rule = \
      '''#lockDialogGroup {
      background: #000F00
      url(file:///usr/share/gnome-shell/theme/dm-background.jpg);
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center; }''' 
    
    parser = cssutils.CSSParser()

    sheet = parser.parseFile('/usr/share/gnome-shell/theme/ubuntu.css')

    rules = sheet.cssRules
    
    for rule in rules:
    
        if type(rule) == cssutils.css.cssstylerule.CSSStyleRule:
        
            if str(rule.selectorText).startswith('#lockDialogGroup'):

                if str(rule.cssText) == original_rule:
                
                    print('Found rule for ID: lockDialogGroup\n')
                    
                    print(rule.cssText)

                    print('\nUpdating rule...'),
                
                    rule.cssText = new_rule
                    
                    with open(
                        '/usr/share/gnome-shell/theme/delete-me.css', 'wb'
                    ) as f:
                        
                        f.write(output)

                    print('done.\n')
                
                    print(rule.cssText)

                    break

                else:

                    print('Original rule has already been edited...')

    output = sheet.cssText.replace(
        '}','}\n'
    )
    
def _update_login_bg_link(bg_link_target_fullpath):

    print('Updating link to gdm login background...')
    
    subprocess.call([
	'rm',
	_GDM_BG_FILEPATH
    ])

    subprocess.call([
        'ln',
        '--symbolic',
	bg_link_target_fullpath,
        _GDM_BG_FILEPATH,
        '--verbose'
    ])

def set_bg(fullpath):
    
    _update_login_bg_css()

    _update_login_bg_link(fullpath)
        







