"""My grub2 module for Python

(C) Ryan James Decker All Rights Reserved

Nothing much to document here yet. When 
  it begins to grow, I'll update 
  accordingly.

Link: https://help.ubuntu.com/community/Grub2/Displays

"""

import os, re, subprocess

GRUB2_COLORS = (
    'black',
    'blue',
    'brown',
    'cyan',
    'dark-gray',
    'green',
    'light-cyan',
    'light-blue',
    'light-green',
    'light-gray',
    'light-magenta',
    'light-red',
    'magenta',
    'red',
    'white',
    'yellow'
)

_ORIGINAL_CONFIG = r'/etc/default/grub.original'

_BACKUP_CONFIG = r'/etc/default/grub.backup'

_LIVE_CONFIG = r'/etc/default/grub'

_original_config_contents = r'''# If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.
# For full documentation of the options in this file, see:
#   info -f grub -n 'Simple configuration'

GRUB_DEFAULT=0
#GRUB_HIDDEN_TIMEOUT=0
#GRUB_HIDDEN_TIMEOUT_QUIET=false
GRUB_TIMEOUT=30
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX=""

# Uncomment to enable BadRAM filtering, modify to suit your needs
# This works with Linux (no patch required) and with any kernel that obtains
# the memory map information from GRUB (GNU Mach, kernel of FreeBSD ...)
#GRUB_BADRAM="0x01234567,0xfefefefe,0x89abcdef,0xefefefef"

# Uncomment to disable graphical terminal (grub-pc only)
#GRUB_TERMINAL=console

# The resolution used on graphical terminal
# note that you can use only modes which your graphic card supports via VBE
# you can see them in real GRUB with the command `vbeinfo'
#GRUB_GFXMODE=1440x900

# Uncomment if you don't want GRUB to pass "root=UUID=xxx" parameter to Linux
#GRUB_DISABLE_LINUX_UUID=true

# Uncomment to disable generation of recovery mode menu entries
#GRUB_DISABLE_RECOVERY="true"

# Uncomment to get a beep at grub start
GRUB_INIT_TUNE="480 440 1"'''

def update_grub2_splash(fullpath):

    if not fullpath.endswith((
        r'.jpg',
        r'.JPG',
        r'.jpeg',
        r'.JPEG',
        r'.png',
        r'.PNG',
        r'.tga',
        r'.TGA'
    )):

        print(r'File must end with ".jpg", ".png", or ".tga"...')
        
        raise TypeError # I don't know how this works :(

    if not os.path.isfile(_ORIGINAL_CONFIG):

        print 'There\'s no backup for the original "{}". Generating the original config as "{}"...\n'.format(
        _LIVE_CONFIG,
        _ORIGINAL_CONFIG
    )
    
    print(_original_config_contents)

    if not os.path.isfile(_BACKUP_CONFIG):

        print 'There\'s no backup for the current "{}". Copying current config to "{}"...\n'.format(
            _LIVE_CONFIG,
            _BACKUP_CONFIG
        )

    existing_live_config_contents = ''

    with open(_LIVE_CONFIG,'r') as file:

        existing_live_config_contents = file.read()

    print(existing_live_config_contents)

    if fullpath.endswith((
        r'.jpg',
        r'.JPG'
    )):

        subprocess.call([
            'ln',
            '-s',
            fullpath,
            '/boot/grub/splash.jpg'
        ])

    if fullpath.endswith((
        r'.jpeg',
        r'.JPEG'
    )):

        subprocess.call([
            'ln',
            '-s',
            fullpath,
            '/boot/grub/splash.jpeg'
        ])
        
    elif fullpath.endswith((
        r'.png',
        r'.PNG'
    )):

        subprocess.call([
            'ln',
            '-s',
            fullpath,
            '/boot/grub/splash.png'
        ])
        
    elif fullpath.endswith((
        r'.tga',
        r'.TGA'
    )):

        subprocess.call([
            'ln',
            '-s',
            fullpath,
            '/boot/grub/splash.tga'
        ])

def update_menu_colors(
    normal_color_pair = ('white','black'),
    highlight_color_pair = ('red','white')
):

    print("Colors supported in grub2:")

    for color in GRUB2_COLORS:

        print(color)

    #print('Notice: ')

    color_normal = normal_color_pair
    color_highlight = highlight_color_pair

    MENU_COLOR_CONFIG = r'/etc/grub.d/05_debian_theme'
        
    menu_color_config_contents = ''

    with open(MENU_COLOR_CONFIG,'r') as file:

        menu_color_config_contents = file.read()

    pattern = re.compile(r'(?<=\n)\s*if\s+\[\s+-z[^f]+fi')

    match_object = pattern.search(menu_color_config_contents)

    match_text = match_object.group()

    replacement_text = \
'''\tif [ -z "${2}" ] && [ -z "${3}" ]; then
\t\t# echo "  true"
\t\techo "    set color_highlight=color1/color2"
\t\techo "    set color_normal=color1/black"
\tfi'''
    
    menu_color_config_updated_text = pattern.sub(
        replacement_text,
        menu_color_config_contents
    )

    print('Replacing this:')
    print(match_text)
    print('WITH THIS:')
    print(replacement_text)
                         
    '''
    Configuration settings (splash image present)

When an image is present GRUB 2 uses only color_normal and color_highlight settings. Themes are recommended when the user wishes to override the default monochromatic color text scheme. The user can add the menu_color_highlight and menu_color_normal settings to system files if desired.

    The color of selected menu entries is set by menu_color_highlight setting.

    The color of non-selected menu entries is set by menu_color_normal setting.

    The second value of the color_normal entry must be set to black for the image to be visible. 

To add menu_color_normal and/or menu_color_highlight values when using a splash image:

    Open /etc/grub.d/05_debian_theme as root.
        Find the following lines:

            if [ -z "${2}" ] && [ -z "${3}" ]; then
                echo "  true"
            fi

        Change the entry to the following, replacing 'color1' and 'color2' to the desired colors. Leave /black as is!

            if [ -z "${2}" ] && [ -z "${3}" ]; then
                # echo "  true"
                echo "    set color_highlight=color1/color2"
                echo "    set color_normal=color1/black"
            fi

    Save the file and run update-grub

    '''














