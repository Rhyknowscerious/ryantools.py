"""My plymouth module for Python

(C) Ryan James Decker All Rights Reserved

Description:
-----------
This is the ryan_tools.ubuntu.plython module.

Changes the plymouth splash theme in Ubuntu 18.04

Classes:
-------
(none)

Functions:
---------
- get_input_from_menu() - This needs to be separated
- splash_menu()
- initramfs_menu()
- reboot_menu()

"""

import subprocess

def get_input_from_menu(message,options):
    
    print(message)

    count = 0

    for option in options:

        count += 1

        print('{count}) {option}'.format(count=count,option=option))

    choice = input('Pick a number: ')

    return choice

def splash_menu():
    
    message = 'Do you want to update your plymouth theme?'

    options = ('yes','no')

    choice = int(\
        get_input_from_menu(message,options) \
    )

    print( \
        'You chose option {}.'.format(choice) \
    )

    if ( \
        choice < 1 or \
        choice > 2    \
    ):
        print('Invalid selection...')
        
    if choice == 1:
        
        print('Changing plymouth theme...')

        subprocess.call([ \
            'update-alternatives',  \
            '--config',             \
            'default.plymouth'      \
        ])

    elif choice == 2:

        print('Plymouth theme update aborted.')

    return choice

def initramfs_menu():
    
    message = 'Do you want to update initramfs?'

    options = ('yes','no')

    choice = int(\
        get_input_from_menu(message,options)\
    )

    print( \
        'You chose option {}.'.format(choice)\
    )

    if ( \
        choice < 1 or \
        choice > 2    \
    ):
        print('Invalid selection...')
        
    elif choice == 1:
        
        print('Updating initramfs..')
        
        subprocess.call([ \
            'update-initramfs',  \
            '-u'                 \
        ])

    elif choice == 2:

        print('Initramfs update aborted.')

    return choice

def reboot_menu():
    
    message = 'Do you want to restart your system?'

    options = ('yes','no')

    choice = int(\
        get_input_from_menu(message,options)\
    )

    print( \
        'You chose option {}.'.format(choice)\
    )

    if ( \
        choice < 1 or \
        choice > 2    \
    ):
        print('Invalid selection...')
        
    elif choice == 1:
        
        print('Starting reboot sequence..')
        
        subprocess.call([ \
            'shutdown',  \
            '-r',        \
            'now'        \
        ])

    elif choice == 2:

        print('Initramfs update aborted.')

    return choice

