"""Ryan's personal Python library

Root Package

(C) Ryan James Decker All Rights Reserved

Disclaimer:
----------
I just started writing this package  
  so it doesn't have a lot to it right 
  now.
  
It will be updated as time moves on.

Sorry in advance for the bad doc strings 
  and non-compliance with version 
  numbering.

Description:
-----------
This is my ryan_tools package for Python

Included Modules:
----------------
None

Included Packages:
------------------
  -tasker
  -ubuntu
  
"""

#__version__ = '2019.09.28.0'
