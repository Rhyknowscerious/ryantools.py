"""Ryan's personal Python library

Package for Tasker (Android)

(C) Ryan James Decker All Rights Reserved

Description:
-----------
This package includes modules for Tasker (the Android app).

Included Modules:
----------------
  -tasker
  -web

Included Packages:
-----------------
None

"""

