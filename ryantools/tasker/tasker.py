"""My Tasker module for Python

(C) Ryan James Decker All Rights Reserved

Description:
-----------
This is the RyanTools.Tasker module.

Its purpose is to create an easy syntax 
  for invoking Tasker features from 
  Python.

Classes:
-------
- Task
- Variable - coming soon

Functions:
---------
- flash()
- notify()

"""

import time

import android

droid = android.Android()
    
def flash(string):
    """Tasker's flash feature"""
    
    droid.makeToast(string)
    
def notify(title,message):
    """Tasker's notification feature..."""
    
    droid.notify(title,message)
    
class Task():
    """Task Object (needs work)"""
    
    SET_VARIABLE = 547
    
    def new_task(self):
        """I don't even know..."""
        
        self.action_cnt = 0
        
        self.extras = {\
            'version_number': '1.0', \
            'task_name': 'task' + str(\
                time.time()\
            ), \
            'task_priority': 9\
        }
            
    def set_var(self, varname, value):
        """Helper method I guess..."""
    
        self.action_cnt += 1
        
        self.extras[\
            'action' + str(\
                self.action_cnt\
            )\
        ] = {\
            'action': self.SET_VARIABLE, \
            'arg:1': varname, \
            'arg:2': value, \
            'arg:3': False, \
            'arg:4': False, \
            'arg:5': False}
    
    def run_task(self):
        """Run a Tasker task...""" 
        
        taskIntent = \
            droid.makeIntent(\
                'net.dinglisch.android.tasker.ACTION_TASK', \
                None, \
                None, \
                self.extras\
            ).result
        
        droid.sendBroadcastIntent(\
            taskIntent\
        )
    
    def set_var_now(self, varname, value):
        """Use this one to actually set a variable in Tasker"""
    
        self.new_task()
        
        self.set_var(varname, value)
        
        self.run_task()

#class Variable():
#
#    def get(self):
#    
#        params = \
#            droid.getIntent().result[u'extras']
#        
#        for key in params.keys(): 
#            if key.startswith('%'):
#                exec 'global ' + key.lstrip('%') 
#                exec key.lstrip('%') + '="' + params[key] + '"'
#    def set(self):
#        pass
        
        
        
        
        
