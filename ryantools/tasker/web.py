"""My web module for Python

(C) Ryan James Decker All Rights Reserved

Nothing much to document here yet. When 
  it begins to grow, I'll update 
  accordingly.
"""

import requests

import android
droid = android.Android()

def httpGet(url):
    return requests.get(url)
