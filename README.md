# ryantools.py

Here's my personal Python library packaged as ryantools for Python version 2.7.

I have sub-packages for Tasker (the Android app) and Ubuntu (Version 18.04).

## Tasker
I test this on Android via SL4A/PY4A and Tasker.

Here's where you can get SL4A and PY4A apk installers: 

https://github.com/kuri65536/python-for-android/blob/master/README.md

Here's where you can get Tasker:

https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm

Notes:
The SL4A and PY4A apk files are not from the Google Play store but Tasker is.

Tasker is not required but SL4A+PY4A are if you want to use Python on the phone. 

Tips:

You can run IDLE from SL4A under the 
  "interpreters" view by selecting 
  whichever Python version you decided on.

If you do decide to use tasker, to execute 
  a python script:
  
  -Put your script in /sdcard/SL4A/scripts.
  -Then create a task.
  -Then create an action "Run SL4A script".
  -Select the script that you put in the
    /sdcard/SL4A/scripts folder.

To add packages/modules to python, put 
them in 

  /sdcard/com.googlecode.pythonforandroid/extras/python

  for example, when I installed all this 
  stuff, the responses package was not 
  included so I had to manually download 
  and copy the module folder over from 
  github. And I had to get the 
  dependencies the same way because I 
  never figured out how to get pip running  
  on Android. 

## Ubuntu

So far, I'm experimenting with theming the system from boot screen to desktop loading.

The modules in the Ubuntu package are included to attempt to create an API to 
apply custom visual theming to the following:

* grub2 boot menu text colors
* grub2 boot menu background image
* plymouth splash image/animation/script
* gdm login screen background image
* gnome3 desktop background image
* gnome3 lock screen background image

I plan to add functions for changing the following:

* logoff/shutdown menu

I don't plan to add functions for changing these:

* window manager theme
* icon theme

Try using gnome-tweaks for that `sudo apt install gnome-tweaks`












